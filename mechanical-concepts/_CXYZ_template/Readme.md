# CONCEPT: CXYZ (nick)
Status: Design/Testing/Retired

---

## Team Lead(s):
|Name|GitLab|
|---|---|
| Example Name | @example |
| | |

## Concept Overview:

### Introduction

### Function

### Mechanical Design

### Electrical

### Manufacturability

### Aesthetic

### Known Problems / Considerations

## Issue Labels:
~"CXYZ"

## Related Issues and Boards:
- Feedback Thread #0

## Other Important Links
