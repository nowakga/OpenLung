# CONCEPT: C8A2
Status: Retired

---

## Team Lead(s):
|Name|GitLab|
|---|---|
| David O'Reilly  | @dor2020  |
| Trevor Smale | @TrevorSmale |

## Concept Overview:

### Introduction

The 8.2 would be using a Nema 23. I have a Nema 17 in the model but the 23 would perform better. The motor should rotate 3 turns per breath. I'm hoping the larger motor will handle the 3 turns. I'm looking forward to seeing how testing goes. In the meantime I'm looking at a simpler design, that takes a different approach with the pushing mechanism.

### Function

### Mechanical Design

### Electrical

### Manufacturability

### Aesthetic

### Known Problems / Considerations

## Issue Labels:
~"CXYZ"

## Related Issues and Boards:
- Feedback Thread #0

## Other Important Links
